﻿using UnityEngine;
using System.Collections;

public class MoveRacket : MonoBehaviour {
	public float speed = 30; // määratav reketi liigutamis kiirus
	public string axis = "Vertical"; // määratav reketi input kontroller

	void FixedUpdate () { // kutsutakse igal uuel framel
		float v = Input.GetAxisRaw(axis); // pärime hetke kontrolleri väärtuse
		GetComponent<Rigidbody2D>().velocity = new Vector2(0, v) * speed; // liigutame reketit vertikaalselt
	}
}