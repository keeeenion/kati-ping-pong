﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Counter : MonoBehaviour {
	private int score = 0; // privaatne muutuja skoori talletamiseks

    // lisame skoorile ühe punkti juurde
	public void plusOne() {
		score++; // lisame ühe punkti juurde
		gameObject.GetComponent<Text> ().text = score.ToString (); // kuvame skoori
	}
}
