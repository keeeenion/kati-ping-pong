﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {
	public float speed = 30; // avalik kiirus muutuja

	void Start() {
		GetComponent<Rigidbody2D>().velocity = Vector2.right * speed; // paneme objekti paremale liikuma seatud kiirusega
	}

    // leiame suuna kuhu pall põrkama peaks
    // mida reketi nurga poole, seda suurem nurga all pall minema lendab
	float hitFactor(Vector2 ballPos, Vector2 racketPos, float racketHeight) {
		return (ballPos.y - racketPos.y) / racketHeight;
	}

    // pall lüüakse keskelt uuesti lahti
	public void resetBall() {
		GetComponent<Rigidbody2D>().transform.position = new Vector2(0, 0); // viime palli kordinaatidele [0;0]
		GetComponent<Rigidbody2D>().velocity = Vector2.right * speed; // paneme palli uuesti liikuma
	}

    // teise collision objektiga kokkupuutel kutsutakse see funktsioon
    void OnCollisionEnter2D(Collision2D col) {

        // kui vasak reket hitboxi puudutas
        if (col.gameObject.name == "RacketLeft") {
			float y = hitFactor(transform.position,
				col.transform.position,
				col.collider.bounds.size.y); // arvutame suuna kuhu pall nüüd põrkama peaks

			Vector2 dir = new Vector2(1, y).normalized; // loome uue vektori

			GetComponent<Rigidbody2D>().velocity = dir * speed; // paneme objekti arvutatud suunda liikuma
		}

        // kui parem reket hitboxi puudutas
        if (col.gameObject.name == "RacketRight") {
			float y = hitFactor(transform.position,
				col.transform.position,
				col.collider.bounds.size.y);

			Vector2 dir = new Vector2(-1, y).normalized;

			GetComponent<Rigidbody2D>().velocity = dir * speed;
		}
	}
}