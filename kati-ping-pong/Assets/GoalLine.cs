﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalLine : MonoBehaviour {
	public Counter counter; // selle värava määratav counter objekt, mis seisu hoiab

	void OnCollisionEnter2D(Collision2D col) {
		if (col.gameObject.name == "Ball") { // kui pall objekt hitboxi puudutab
			counter.plusOne (); // kutsume counteri skoori uuendama
			GameObject.Find ("Ball").GetComponent<Ball>().resetBall (); // kutsume palli oma funktsiooni, mis palli uuesti keskelt lahti lööb
		}
	}
}
